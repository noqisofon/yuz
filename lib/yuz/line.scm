(define-module (yuz line)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-13)
  #:export (<line>
            make-line
            line?
            line-prev
            set-line-prev
            line-next
            set-line-next
            line-text
            set-line-text
            line-plist
            set-line-plist
            line-alive?
            line-text-ref
            line-length))

(define-record-type <line>
  (make-line prev next text)
  line?
  (prev      line-prev     set-line-prev)
  (next      line-next     set-line-next)
  (text      line-text     set-line-text)
  (plist     line-plist    set-line-plist))


(define (line-alive? a-line)
  ""
  (not (null? (line-text a-line))))

(define (line-text-ref a-line an-index)
  "行 a-line の an-index 目の文字を返します。"
  (if (equal? an-index (line-length a-line))
      #\newline
      (string-ref (line-text a-line) an-index)))

(define (line-length a-line)
  "行 a-line に含まれる文字列の文字列数を返します。"
  (string-length (line-text a-line)))

