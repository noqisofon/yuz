(define-module (yuz basic)
  #:use-module (yuz line)
  #:use-module (yuz point)
  #:export (
            first-line?
            last-line?
            start-line?
            ))

(define (first-line? a-point)
  "`a-point` が最初の行なら真、それ以外なら偽を返します。"
  (null? (line-prev (point-line a-point))))

(define (last-line? a-point)
  "`a-point` が最後の行なら真、それ以外なら偽を返します。"
  (null? (line-next (point-line a-point))))

(define (start-line? a-point)
  "`a-point` が行頭なら真、それ以外なら偽を返します。"
  (zero? (point-char-position a-point)))

(define (end-line? a-point)
  "`a-point` が行末なら真、それ以外なら偽を返します。"
  (equal? (point-char-position a-point)
          (line-length (point-line a-point))))

(define (start-buffer? a-point)
  "`a-point` がバッファの最初の位置なら真、それ以外なら偽を返します。"
  (and (first-line? a-point)
       (start-line? a-point)))
